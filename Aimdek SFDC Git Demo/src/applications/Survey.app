<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <formFactors>Large</formFactors>
    <label>Survey</label>
    <tab>Survey__c</tab>
    <tab>Survey_Question__c</tab>
    <tab>SurveyResponse__c</tab>
    <tab>SurveyUnsubscriber__c</tab>
    <tab>Program__c</tab>
    <tab>AAAClub__c</tab>
    <tab>Problem_Type__c</tab>
    <tab>Service_Tracker_Detail__c</tab>
    <tab>Close_the_loop__c</tab>
</CustomApplication>
