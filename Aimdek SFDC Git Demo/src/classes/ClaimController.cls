public with sharing class ClaimController {


    string claimFields {set;get;}
    string progCode {set;get;}
    string progExtCode {set;get;}
    string progPromoCode {set;get;}
    
    public Claim__c claim {set;get;}
    public string logourl {set;get;}
    public string submiturl {set;get;}
    public string queryurl {set;get;}
    public string faqurl {set;get;}

    public boolean isSubmit {set;get;}
    public boolean isQuery {set;get;}
    public boolean isFAQ {set;get;}
    public boolean isDone {set;get;}

    public boolean isRSAClaim {set;get;}
    public boolean isTrExClaim {set;get;}
    

    public transient  blob receipt1 {set;get;}
    public string receiptName1    {get;set;}
    public transient  blob receipt2 {set;get;}
    public string receiptName2    {get;set;}
    public transient blob receipt3 {set;get;}
    public string receiptName3   {get;set;}


    public transient  blob mealreceipt1 {set;get;}
    public string mealreceiptName1    {get;set;}
    public transient  blob mealreceipt2 {set;get;}
    public string mealreceiptName2    {get;set;}
    public transient blob mealreceipt3 {set;get;}
    public string mealreceiptName3   {get;set;}

    public transient  blob hotelreceipt1 {set;get;}
    public string hotelreceiptName1    {get;set;}
    public transient  blob hotelreceipt2 {set;get;}
    public string hotelreceiptName2    {get;set;}

    public transient  blob rentalreceipt1 {set;get;}
    public string rentalreceiptName1    {get;set;}
    public transient  blob rentalreceipt2 {set;get;}
    public string rentalreceiptName2    {get;set;}

    public transient  blob workorder {set;get;}
    public string workorderName    {get;set;}


    public string queryresult {set;get;}
    
    public string qname {set;get;}
    public string qemail{set;get;}
    public string uid {set;get;}
    
    public boolean VINNotFound {set;get;}
    public boolean WrtyExpired {set;get;}
    
    public string claimNos {set;get;}
   
   
   
    public ClaimController()
    {
        
    }
    
    public void init()
    {
            logourl = 'https://c.cs20.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=015m00000010hEn&oid=00Dm00000004gir';
            submiturl = 'https://c.cs20.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=015m00000010hLo&oid=00Dm00000004gir';
            queryurl = 'https://c.cs20.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=015m00000010hLt&oid=00Dm00000004gir';
            faqurl = 'https://c.cs20.content.force.com/servlet/servlet.ImageServer/servlet/servlet.ImageServer?id=015m00000010hLy&oid=00Dm00000004gir';
    
            isFAQ = true;
            progCode = '503';
            progExtCode = '';
            progPromoCode = '';
            claim = new Claim__c();     
            getClaimFields();
            
    }
    
    public void getClaimFields() {
        claimFields='';
        for(Schema.SObjectField f : Claim__c.SObjectType.getDescribe().fields.getMap().values()) {
            claimFields += (f.getDescribe().getName() + ',');
        }
        claimFields=claimFields.left(claimFields.length()-1);
    }
    public void SubmitNew()
    {
        isSubmit = true;
        isQuery = false;
        isFAQ=false;    
        isDone = false;
        VINNotFound = false;
        isRSAClaim = false;
        isTrExClaim = false;
        claim = new claim__c();
    }

    public void QueryRequest()
    {
        isSubmit = false;
        isQuery = true;
        isFAQ=false;
        isDone = false;
        queryresult = '';
    }


    public void Query()
    {
        try
        {
            
        //claim = [select createddate,status__c,name,lastname__c,firstname__c from claim__c where name = :claim.name]; // and firstname__c =:claim.firstname__c and lastname__c =:claim.lastname__c and phone__c =:claim.phone__c];
            String qr = 'Select ' + claimFields + ' from Claim__c where name=\''+string.escapeSingleQuotes(qname)+'\' and email__c=\'' + string.escapeSingleQuotes(qemail) + '\' LIMIT 1';
            
            system.debug(qr);
            claim = database.query(qr);
            
            if (claim != null)
            {
                queryresult = 'Claim Received on ' + string.valueof(claim.createddate) + '\nStatus: ' + claim.status__c;
            }
            else
            {
                queryresult = 'NO such claim found. Please make sure all informations are correct.';
            }
            }
        catch(System.exception ex)
        {
            queryresult = 'NO such claim found. Please make sure all informations are correct.';
        }
    }

    public void FAQ()
    {
        isSubmit = false;
        isQuery = false;
        isFAQ=true;
        isDone = false;
    }
    
    public void Submit()
    {
        claimNos = '';
        claim.program__c = '503 - Kia Roadside Assistance';     

        if (this.isRSAClaim)
        {
            Claim__c claim1 = new Claim__c();
            claim1 = claim.clone(true);
            claim1.ETECause__c=null;
            claim1.ETEDate__c=null;
            claim1.ETEHotel__c=null;
            claim1.ETELocation__c=null;
            claim1.ETEMeal__c=null;
            claim1.ETEOther__c=null;
            claim1.ETEOtherCause__c=null;
            claim1.ETERental__c=null;
            claim1.ETETrans__c=null;
            claim1.ClaimType__c = 'Roadside Service Claim';
            
            Map<String, Schema.SObjectField> map1 = Schema.SObjectType.Claim__c.fields.getMap();
            for(String fieldName : map1.keySet()) 
            {
                if(map1.get(fieldName).getDescribe().isUpdateable()) 
                {
                    try
                    {
                        string st = string.valueof(claim1.get(fieldname)).toUpperCase();
                        claim1.put(fieldName , st);
                    }
                    catch(System.exception ex){}
                }
            }
               
            insert claim1;

            if ((receiptname1 != null) && (receiptname1 != ''))
            {
                Attachment attachment1 = new Attachment();
                
                try
                {       
                    attachment1.Body = receipt1;
                    attachment1.Name = receiptname1;
                    attachment1.ParentId = claim1.id;
                    insert attachment1;
                }
                catch(System.Exception e)
                {
                }
                receipt1= null;
            }
            
            if ((receiptname2 != null) && (receiptname2 != ''))
            {
                
                Attachment attachment2 = new Attachment();
                
                try
                {       
                attachment2.Body = receipt2;
                attachment2.Name = receiptname2;
                attachment2.ParentId = claim1.id;
                insert attachment2;
                }
                catch(System.Exception e)
                {
                }
                receipt2= null;
            }
            
            if ((receiptname3 != null) && (receiptname3 != ''))
            {
                Attachment attachment3 = new Attachment();
                
                try
                {       
                attachment3.Body = receipt3;
                attachment3.Name = receiptname3;
                attachment3.ParentId = claim1.id;
                insert attachment3;
                }
                catch(System.Exception e)
                {
                }
                receipt3= null;
            }
    
            string qr = 'Select ' + claimFields + ' from Claim__c where id =\''+claim1.id +'\'';
            
            system.debug('after submit:' + qr);
            claim1 = database.query(qr);
            claimNos = claim1.name;
    
        }
        

        if (this.isTrExClaim)
        {
            Claim__c claim2 = new Claim__c();
            claim2 = claim.clone(true);
            claim2.ProviderCity__c=null;
            claim2.ProviderName__c=null;
            claim2.ProviderPhone__c=null;
            claim2.ProviderPostalcode__c=null;
            claim2.ProviderProvince__c=null;
            claim2.ProviderStreet__c=null;
            claim2.Service__c=null;
            claim2.ServiceDate__c=null;
            claim2.Amount__c=null;
            claim2.ClaimType__c = 'Emergency Travel Expense Claim';
            
            Map<String, Schema.SObjectField> map1 = Schema.SObjectType.Claim__c.fields.getMap();
            for(String fieldName : map1.keySet()) 
            {
                if(map1.get(fieldName).getDescribe().isUpdateable()) 
                {
                    try
                    {
                        string st = string.valueof(claim2.get(fieldname)).toUpperCase();
                        claim2.put(fieldName , st);
                    }
                    catch(System.exception ex){}
                }
            }
               
            insert claim2;
            
            if ((mealreceiptname1 != null) && (mealreceiptname1 != ''))
            {
                Attachment attachment4 = new Attachment();
                
                try
                {       
                attachment4.Body = mealreceipt1;
                attachment4.Name = mealreceiptname1;
                attachment4.ParentId = claim2.id;
                insert attachment4;
                }
                catch(System.Exception e)
                {
                }
                mealreceipt1= null;
            }
    
    
            if ((mealreceiptname2 != null) && (mealreceiptname2 != ''))
            {
                Attachment attachment5 = new Attachment();
                
                try
                {       
                attachment5.Body = mealreceipt2;
                attachment5.Name = mealreceiptname2;
                attachment5.ParentId = claim2.id;
                insert attachment5;
                }
                catch(System.Exception e)
                {
                }
                mealreceipt2= null;
            }
    
            if ((mealreceiptname3 != null) && (mealreceiptname3 != ''))
            {
                Attachment attachment6 = new Attachment();
                
                try
                {       
                attachment6.Body = mealreceipt3;
                attachment6.Name = mealreceiptname3;
                attachment6.ParentId = claim2.id;
                insert attachment6;
                }
                catch(System.Exception e)
                {
                }
                mealreceipt3= null;
            }
    
    
    
            if ((hotelreceiptname1 != null) && (hotelreceiptname1 != ''))
            {
                Attachment attachment4 = new Attachment();
                
                try
                {       
                attachment4.Body = hotelreceipt1;
                attachment4.Name = hotelreceiptname1;
                attachment4.ParentId = claim2.id;
                insert attachment4;
                }
                catch(System.Exception e)
                {
                }
                hotelreceipt1= null;
            }
    
    
            if ((hotelreceiptname2 != null) && (hotelreceiptname2 != ''))
            {
                Attachment attachment5 = new Attachment();
                
                try
                {       
                attachment5.Body = hotelreceipt2;
                attachment5.Name = hotelreceiptname2;
                attachment5.ParentId = claim2.id;
                insert attachment5;
                }
                catch(System.Exception e)
                {
                }
                hotelreceipt2= null;
            }
    
    
            if ((rentalreceiptname1 != null) && (rentalreceiptname1 != ''))
            {
                Attachment attachment4 = new Attachment();
                
                try
                {       
                attachment4.Body = rentalreceipt1;
                attachment4.Name = rentalreceiptname1;
                attachment4.ParentId = claim2.id;
                insert attachment4;
                }
                catch(System.Exception e)
                {
                }
                rentalreceipt1= null;
            }
    
    
            if ((rentalreceiptname2 != null) && (rentalreceiptname2 != ''))
            {
                Attachment attachment5 = new Attachment();
                
                try
                {       
                attachment5.Body = rentalreceipt2;
                attachment5.Name = rentalreceiptname2;
                attachment5.ParentId = claim2.id;
                insert attachment5;
                }
                catch(System.Exception e)
                {
                }
                rentalreceipt2= null;
            }
        
            if ((workordername != null) && (workordername != ''))
            {
                Attachment attachment4 = new Attachment();
                
                try
                {       
                attachment4.Body = workorder;
                attachment4.Name = 'Work Order';
                attachment4.ParentId = claim2.id;
                insert attachment4;
                }
                catch(System.Exception e)
                {
                }
                workorder = null;
            }
        
        
            string qr = 'Select ' + claimFields + ' from Claim__c where id =\''+claim2.id +'\'';
            
            system.debug('after submit:' + qr);
            claim2 = database.query(qr);
            
            if (claimNos != '')
                claimNos += ',' + claim2.name;
            else
                claimNos = claim2.name;
        }
        
        
        //claim = [select name from claim__c where id=:claim.id];
        
        isSubmit = false;
        isQuery = false;
        isFAQ=false;
        isDone = true;
        
    }
    
    public void Clear()
    {
        claim = new claim__c();
    }
    
    public void vinChanged()
    {
        VINNotFound = true;
        WrtyExpired = true;
        VIN__c vin = null;
        
        string qstr = 'select id,Base_Warranty_Start__c,Base_Warranty_End__c,Base_Warranty_Max_Odometer__c from vin__c where name = \'' + claim.vin__c + '\' and program__r.Program_Code__c = \'' + progCode + '\' and inactive__c != true limit 1'; 
        
        system.debug('base query :' + qstr);
        
        try
        {
            vin = database.query(qstr);
        }
        catch(System.Exception ex)
        {}

        if (vin != null)
        {
            VINNotFound = false; 
            
            if (date.valueof(vin.Base_Warranty_End__c) > system.today())
                WrtyExpired = false;
            else
                return;
        }
        
                
        if ((VINNotFound && progExtCode != '') || (!VINNotFound && WrtyExpired))
        {
            qstr = 'select id,Extended_Warranty_Start__c,Extended_Warranty_End__c,Extended_Warranty_Max_Odometer__c from vin__c where name = \'' + claim.vin__c + '\' and Extended_Program__r.Program_Code__c = \'' + progExtCode + '\' and inactive__c != true limit 1'; 
            system.debug('ext query :' + qstr);
            try
            {
                vin = database.query(qstr);
            }
            catch(System.Exception ex)
            {}
            
            if (vin != null)
            {
                VINNotFound = false; 
                
                if (date.valueof(vin.Extended_Warranty_End__c) > system.today())
                    WrtyExpired = false;
                else
                    return;
            }
        }
        
        
        if ((VINNotFound && progExtCode != '') || (!VINNotFound && WrtyExpired))
        {
            qstr = 'select id,Promo_Start__c,Promo_End__c,Promo_Max_Odometer__c from vin__c where name = \'' + claim.vin__c + '\' and Promo_Program__r.Program_Code__c = \'' + progPromoCode + '\' and inactive__c != true limit 1'; 
            system.debug('promo query :' + qstr);
            try
            {
                vin = database.query(qstr);
            }
            catch(System.Exception ex)
            {}
        
            if (vin != null)
            {
                VINNotFound = false; 
                
                if (date.valueof(vin.Promo_End__c) > system.today())
                    WrtyExpired = false;
                else
                    return;
            }
        }
        
        
    }
}