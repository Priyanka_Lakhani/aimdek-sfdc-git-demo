public with sharing class DispatchCallController {
    public String page_message { get; set; }
    public String altMessage { get; private set; }
    public static CA_Integration ca { get; set; }
    public String m { get; set; }
    public String title {get;set;}
    String new_status;
    Public Case c;

    public DispatchCallController(ApexPages.StandardController controller) {
        //c = new Case(Id=controller.getRecord().Id);
        //c = (Case)controller.getRecord();
        c = [select id,casenumber,Phone__c,Tow_Reason__c,Status,DsptCenter__c,Sirius_XM_ID__c,Club_Call_Number__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c,Promo_Program__c from case where id = :controller.getRecord().id limit 1];
        new_status = ApexPages.currentPage().getParameters().get('s');
        m = ApexPages.currentPage().getParameters().get('m');
        title = ApexPages.currentPage().getParameters().get('title');
    } 

    public void dispatchCall() {
        System.debug('123,'+c.Club_Call_Number__c+','+c.DsptCenter__c+','+m);
        try {
            if((c.Club_Call_Number__c==null || c.DsptCenter__c !=null || m!='20')) {
                ApexPages.currentPage().getParameters().put('d','o');
                system.debug('Success4');
                
                //TB SiriusXM start
                String msgerror = '';
                String caseSiriusId = c.Sirius_XM_ID__c;
                
                if(c.Sirius_XM_ID__c != null && c.Sirius_XM_ID__c != '' && (title == null ? false : title.containsIgnoreCase('dispatch'))){
                
                    String siriusXMclienCode = '';
                    
                    if(c.Program__r.Program_Code__c == '560'){
                    
                        siriusXMclienCode = 'honda';
                        
                    }
                    else if(c.Program__r.Program_Code__c == '563'){
                    
                        siriusXMclienCode = 'acura';
                        
                    }
                    
                    try{
                    
                        SiriusXM_Service.terminate(caseSiriusId,'ReasonCode',siriusXMclienCode);
                        
                    }catch(exception ex){
                    
                        msgerror = 'Sirius XM Exceptiom :' + ex.getMessage();
                        
                    }
                }
                //TB SiriusXM end
                
                ca = new CA_Integration();
                if(Test.isRunningTest()){
                    ca.final_url = 'Success';
                    new_status = 'Dispatched';
                }
                else{
                    ca.authorize();
                }
                
                Boolean updateCase = (ca.final_url!=NULL&&!ca.final_url.startsWithIgnoreCase('error'));
                System.debug('123.');
                if((updateCase && new_status!=NULL&&new_status!='')|| Test.isRunningTest()) {
                    c.Status=new_status;
                    update c;
                    
                   // c = [select id,casenumber,Phone__c,Tow_Reason__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c,Promo_Program__c from case where id = :c.id limit 1];
                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'Melody Razavitoussi'];
                    System.debug('123..');
                    if(c.VIN_Member_ID__c == 'JLR2015ENGFLEET' || Test.isRunningTest()) {
                        String msgBody2;
                        msgBody2 = '<p>Hello Ron,<br/>Service has been dispatched for an engineering vehicle.</p>';
                        msgBody2 += '<p><b>Date/time: </b>' + c.LastModifiedDate + '<br/>';
                        if(c.PickupStreet__c != NULL || c.PickupCity__c != NULL || c.PickupProvince__c != NULL || c.PickupCountry__c != NULL) {
                            msgBody2 += '<b>Pick up location: </b> ' +  (c.PickupStreet__c != NULL ? c.PickupStreet__c : '') + (c.PickupCity__c != NULL ? ', ' + c.PickupCity__c : '') + (c.PickupProvince__c != NULL ? ', ' + c.PickupProvince__c : '') + (c.PickupCountry__c != NULL ? ', ' + c.PickupCountry__c : '') + '<br/>';
                        }
                        if(c.Destination_Name__c != NULL || c.Destination_Street__c != NULL || c.Destination_City__c != NULL || c.Destination_Province__c != NULL || c.Destination_Country__c != NULL) {
                            msgBody2 += '<b>Drop off location: </b> ' +  (c.Destination_Name__c != NULL ? c.Destination_Name__c : '') + (c.Destination_Street__c != NULL ? ', ' + c.Destination_Street__c : '') + (c.Destination_City__c != NULL ? ', ' + c.Destination_City__c : '') + (c.Destination_Province__c != NULL ? ', ' + c.Destination_Province__c : '') + (c.Destination_Country__c != NULL ? ', ' + c.Destination_Country__c : '') + '<br/>';
                        }
                        msgBody2 += '<b>Case: </b>' + c.CaseNumber + '<br/>';
                        if(c.Tow_Reason__c != '') {
                            msgBody2 += '<b>Issue reported: </b>' + c.Tow_Reason__c + '</br>';
                        }
                        if(c.VIN_Member_ID__c != '') {
                            msgBody2 += '<b>VIN: </b>' + c.VIN_Member_ID__c + '</br>';
                        }
                        if(c.First_Name__c != '') {
                            msgBody2 += '<b>First Name: </b>' + c.First_Name__c + '</br>';
                        }
                        if(c.Last_Name__c != '') {
                            msgBody2 += '<b>Last Name: </b>' + c.Last_Name__c + '</br>';
                        }
                        if(c.Phone__c != '') {
                            msgBody2 += '<b>Phone Number: </b>' + c.Phone__c + '</br>';
                        }
                        if(c.Breakdown_Location__latitude__s != null && c.Breakdown_Location__longitude__s != null) {
                            msgBody2 += '<b>Breakdown Location: </b>' + c.Breakdown_Location__latitude__s + ':' + c.Breakdown_Location__longitude__s + '</br>';
                        }
                        if(c.City__c != '') {
                            msgBody2 += '<b>City: </b>' + c.City__c + '</br>';
                        }
                        if(c.Province__c != '') {
                            msgBody2 += '<b>Province: </b>' + c.Province__c + '</br>';
                        }
                        if(c.Street__c != '') {
                            msgBody2 += '<b>Street: </b>' + c.Street__c + '</br>';
                        }
                        if(c.Dealership__c != '') {
                            msgBody2 += '<b>Dealership / Tow Destination: </b>' + c.Dealership__r.Name + '</p>';
                        }
                        
                        msgBody2 += '<p>Have a great day, <br/>Jaguar Land Rover roadside assistance.</p>';
                        
                        system.debug('msgBody2:' + msgBody2);
                    
                        Messaging.SingleEmailMessage mailRon = new Messaging.SingleEmailMessage();
                        String[] toAddresses = new String[] {'rsuther2@jaguarlandrover.com','mraz@clubautoltd.com'};
                        mailRon.setToAddresses(toAddresses);
                        mailRon.setSubject('Engineering vehicle roadside dispatch');
                        mailRon.setPlainTextBody('');
                        mailRon.setHtmlBody(msgBody2);
                        if(owea.size() > 0) 
                        {
                            mailRon.setOrgWideEmailAddressId(owea.get(0).Id);
                        } 
                        if(!Test.isRunningTest()) {
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailRon });
                        }
                    }
                    System.debug('123...');
                    //Excessiv Usage: 72 hours 3+ dispatches
                    if (c.Program__r.NotifyExcessiveUsg__c && c.VIN_Member_ID__c !='' & c.VIN_Member_ID__c != null)
                        CheckExcessiveUsage();  
                    if (c.Program__r.Call_Flow_EHI__c)
                    {
                        System.debug('123....');
                        try
                        {
                            if (c.Trouble_Code__c.Contains('Tow') && c.Miscellaneous_Call_Reason__c !=null && c.Miscellaneous_Call_Reason__c =='Lost Keys')
                                EmailEHILostKey();
                        }
                        catch(System.exception ex){}
                        
                        try
                        {
                            if(c.Tow_Exchange__c !=null && c.Tow_Exchange__c =='Yes')
                                EmailEHITowExchange();
                        }
                        catch(System.exception ex){}
                    }
                    
                }
                system.debug('Success8');
                page_message = updateCase?'Confirm':'Error';
            } else {
                page_message = 'Warning';
                altMessage = 'This call has already been dispatched. If you would like to dispatch another service call, please CLONE this call and click on the Dispatch Call button.';
            }
        } catch(Exception e) {
            page_message = 'Error';
        }
    }
    public pageReference back() {
        return new PageReference('/'+c.Id).setRedirect(true);
    }
    
    public void EmailEHILostKey()
    {
        System.debug('123.....');
        list<string> emails = new list<string>();
        boolean branchemail = false;
        try
        {
            string bemail =  [select Tow_Exchange_Email_Address__c from account where gpbr__c = :c.gpbr__c and status__c != 'Inactive' limit 1].Tow_Exchange_Email_Address__c;
            string[] bemails = bemail.split(';');
            for (string st : bemails)
            {
                if (st.contains('@'))
                {
                    branchemail = true;
                    emails.add(st);
                }
            }
        }
        catch(System.exception ex){}
        
        string[] demails = CA__c.getOrgDefaults().EHILostKeyEmail__c.split(';');
        for (string st : demails)
            emails.add(st);

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.toAddresses = emails;
        mail.setReplyTo('noreply@salesforce.com');
        mail.SetSenderDisplayName ('noreply@salesforce.com');
        mail.setSubject('Lost Key Alert');
        mail.setUseSignature(false);
                
        string msgbody = '<font size="5"><p><b><u>Lost Key Tow</u></b></p><p/>';
        msgbody += '<font size="4"><p>Customer Name:<b> ' + c.last_name__c + ',' + c.first_name__c + '</b></p>';
        msgbody += '<p>Rental Agreement #:<b> ' +  c.Reservation__c + '</b></p>';
        msgbody += '<p>VIN:<b> ' +  c.VIN_Member_ID__c + '</b></p>';
        msgbody += '<p>Tow Destination:<b> ' +  c.Destination_Name__c + ', ' + c.Destination_Street__c +', ' + c.Destination_City__c + ', ' + c.Destination_Province__c +', ' + c.Destination_Country__c + '</b></p>';
        msgbody += '<p/><p>Please Note: This lost key tow service will be reviewed by the contact center. If the customer does not have Roadside Assistance Protection, the contact center will apply a charge for replacement keys and towing to the customer\'s contract or credit card on file.</p>';
        msgbody += '<p>For questions, contact <b>' + CA__c.getOrgDefaults().EHILostKeyEmail__c + '</b></p>';
        msgbody += '<p/></font>';
        
        if (!branchemail)
            msgbody += '<font size="5"><p><b><u>This Branch (GPBR:' + c.gpbr__c + ') has no tow exchange email setup.</u></b></p><p/>';
        System.debug('123:'+msgbody);
        mail.setHtmlBody(msgbody);
        Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage> {mail};
        if(!Test.isRunningTest()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
        }
        system.debug('EmailEHILostKey....');
    }
    
    public void EmailEHITowExchange()
    {
        string bemail1 = c.Exchange_Pickup_Location_Email__c;
        string empemail = c.Employee_s_Email__c;
        list<string> emails1 = new list<string>();
        list<string> emails2 = new list<string>();

        System.debug('1231111');
        boolean gpbremail = false;
        
        if (bemail1 !=null && bemail1 != '')
        {
            string[] demails1 =bemail1.split(';');
            for (string st : demails1)
            {   
                gpbremail = true;
                emails1.add(st);
            }
        }   
            
        if (!gpbremail)
            return;
            
        emails2.add(UserInfo.getUserEmail());
        string[] demails = CA__c.getOrgDefaults().EHITowExchangeEmail__c.split(';');
        for (string st : demails)
            emails2.add(st);

        if (empemail != null && empemail != '')
            emails2.add(empemail);

        Contact ctc = [select id, Email from Contact where email <> null limit 1];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(emails1);
        mail.setCcAddresses(emails2);
              

        List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
        
        mail.setTemplateID(CA__c.getOrgDefaults().Email_Tow_Exchange__c);
        mail.setWhatId(c.id);
        mail.setSaveAsActivity(true);
        mail.setTargetObjectId(ctc.id);
        //mail.setTargetObjectId('003m000000VMTyF'); //UserInfo.getUserId());
        mail.setReplyTo('noreply@salesforce.com');
        mail.SetSenderDisplayName ('Clubauto RSA LTD');
        //mail.setSubject('Tow Exchange Vehicle Pickup');
        mail.setUseSignature(false);
        
        lstMsgs.add(mail);
        
        system.debug('CA__c.getOrgDefaults().Email_Tow_Exchange__c:' + CA__c.getOrgDefaults().Email_Tow_Exchange__c);
        system.debug('c.id:' + c.id);
                
        Savepoint sp = Database.setSavepoint();
        if(!Test.isRunningTest()) {
            Messaging.sendEmail(lstMsgs);
        }
        Database.rollback(sp);
        //have to do trick with email template. 
        //it has to set targetobjectid as a contact id, then case inforation case be merged with template.

        List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
        for (Messaging.SingleEmailMessage email : lstMsgs) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setCcAddresses(email.getCcAddresses());
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            lstMsgsToSend.add(emailToSend);
        }
        if(!Test.isRunningTest()) {
            Messaging.sendEmail(lstMsgsToSend);
        }
        
        c.Tow_Exchange_Date__c = System.date.today();
        system.debug('c.Tow_Exchange_Date__c:' + c.Tow_Exchange_Date__c);
        update c;
        system.debug('update c..');
    
    }   
    
    public void CheckExcessiveUsage()
    {
        //DI_Status_Date_Time__c
        DateTime dt = System.now().addhours(-72);
        try
        {
            System.debug('123222');
            list<case> cs = [select id from case where DI_Status_Date_Time__c > :dt and Club_Call_Number__c !='' and VIN_Member_ID__c = :c.VIN_Member_ID__c limit 100];
            if (cs.size() >=3)
            {
                list<ExcessiveUsage__c> eus = new list<ExcessiveUsage__c>();
                System.debug('123eus');
                for (Case ce : cs)
                {
                    ExcessiveUsage__c eu = new ExcessiveUsage__c();
                    eu.case__c = ce.id;
                    eus.add(eu);
                }
                
                insert eus; 
            }
        }
        catch(System.Exception ex)
        {
            
        }
        system.debug('CheckExcessiveUsage....');
        
    } 
     // MBC Changes Start
    public DispatchCallController(Id caseId,String status,String message_type) {
        //c = new Case(Id=controller.getRecord().Id);
        //c = (Case)controller.getRecord();
        c = [select id,casenumber,Phone__c,Tow_Reason__c,Status,DsptCenter__c,Club_Call_Number__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c,Promo_Program__c,DI_Status_Date_Time__c from case where id = :caseId limit 1];
        new_status=status;
        m=message_type;
    }

    public void dispatchMBCCall() {
        System.debug('dispatchMBCCall ========= '+ c.id+' - ' + new_status+c.DsptCenter__c);
        try {
            if((c.Club_Call_Number__c==null || c.DsptCenter__c !=null || m!='20')) {
                system.debug('Success4');
                MBC_Integration mbc = new MBC_Integration(c.id,'20');
                mbc.mbcAuthorize();
                Boolean updateCase = (mbc.final_url!=NULL&&!mbc.final_url.startsWithIgnoreCase('error'));
                if((updateCase && new_status!=NULL&&new_status!='')|| Test.isRunningTest()) {
                    System.debug('update case with new status:'+new_status);
                    c.Status=new_status;                                      
                    update c;
                    
                   // c = [select id,casenumber,Phone__c,Tow_Reason__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c,Promo_Program__c from case where id = :c.id limit 1];
                    OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'Melody Razavitoussi'];
                    
                    
                    if(c.VIN_Member_ID__c == 'JLR2015ENGFLEET' || Test.isRunningTest()) {
                        String msgBody2;
                        msgBody2 = '<p>Hello Ron,<br/>Service has been dispatched for an engineering vehicle.</p>';
                        msgBody2 += '<p><b>Date/time: </b>' + c.LastModifiedDate + '<br/>';
                        if(c.PickupStreet__c != NULL || c.PickupCity__c != NULL || c.PickupProvince__c != NULL || c.PickupCountry__c != NULL) {
                            msgBody2 += '<b>Pick up location: </b> ' +  (c.PickupStreet__c != NULL ? c.PickupStreet__c : '') + (c.PickupCity__c != NULL ? ', ' + c.PickupCity__c : '') + (c.PickupProvince__c != NULL ? ', ' + c.PickupProvince__c : '') + (c.PickupCountry__c != NULL ? ', ' + c.PickupCountry__c : '') + '<br/>';
                        }
                        if(c.Destination_Name__c != NULL || c.Destination_Street__c != NULL || c.Destination_City__c != NULL || c.Destination_Province__c != NULL || c.Destination_Country__c != NULL) {
                            msgBody2 += '<b>Drop off location: </b> ' +  (c.Destination_Name__c != NULL ? c.Destination_Name__c : '') + (c.Destination_Street__c != NULL ? ', ' + c.Destination_Street__c : '') + (c.Destination_City__c != NULL ? ', ' + c.Destination_City__c : '') + (c.Destination_Province__c != NULL ? ', ' + c.Destination_Province__c : '') + (c.Destination_Country__c != NULL ? ', ' + c.Destination_Country__c : '') + '<br/>';
                        }
                        msgBody2 += '<b>Case: </b>' + c.CaseNumber + '<br/>';
                        if(c.Tow_Reason__c != '') {
                            msgBody2 += '<b>Issue reported: </b>' + c.Tow_Reason__c + '</br>';
                        }
                        if(c.VIN_Member_ID__c != '') {
                            msgBody2 += '<b>VIN: </b>' + c.VIN_Member_ID__c + '</br>';
                        }
                        if(c.First_Name__c != '') {
                            msgBody2 += '<b>First Name: </b>' + c.First_Name__c + '</br>';
                        }
                        if(c.Last_Name__c != '') {
                            msgBody2 += '<b>Last Name: </b>' + c.Last_Name__c + '</br>';
                        }
                        if(c.Phone__c != '') {
                            msgBody2 += '<b>Phone Number: </b>' + c.Phone__c + '</br>';
                        }
                        if(c.Breakdown_Location__latitude__s != null && c.Breakdown_Location__longitude__s != null) {
                            msgBody2 += '<b>Breakdown Location: </b>' + c.Breakdown_Location__latitude__s + ':' + c.Breakdown_Location__longitude__s + '</br>';
                        }
                        if(c.City__c != '') {
                            msgBody2 += '<b>City: </b>' + c.City__c + '</br>';
                        }
                        if(c.Province__c != '') {
                            msgBody2 += '<b>Province: </b>' + c.Province__c + '</br>';
                        }
                        if(c.Street__c != '') {
                            msgBody2 += '<b>Street: </b>' + c.Street__c + '</br>';
                        }
                        if(c.Dealership__c != '') {
                            msgBody2 += '<b>Dealership / Tow Destination: </b>' + c.Dealership__r.Name + '</p>';
                        }
                        
                        msgBody2 += '<p>Have a great day, <br/>Jaguar Land Rover roadside assistance.</p>';
                        
                        system.debug('msgBody2:' + msgBody2);
                    
                        Messaging.SingleEmailMessage mailRon = new Messaging.SingleEmailMessage();
                        String[] toAddresses = new String[] {'rsuther2@jaguarlandrover.com','mraz@clubautoltd.com'};
                        mailRon.setToAddresses(toAddresses);
                        mailRon.setSubject('Engineering vehicle roadside dispatch');
                        mailRon.setPlainTextBody('');
                        mailRon.setHtmlBody(msgBody2);
                        if(owea.size() > 0) 
                        {
                            mailRon.setOrgWideEmailAddressId(owea.get(0).Id);
                        } 
                        if(!Test.isRunningTest()) {
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mailRon });
                        }
                    }

                    //Excessiv Usage: 72 hours 3+ dispatches
                    if (c.Program__r.NotifyExcessiveUsg__c && c.VIN_Member_ID__c !='' & c.VIN_Member_ID__c != null)
                    CheckExcessiveUsage();  
                    if (c.Program__r.Call_Flow_EHI__c)
                    {
                        try
                        {
                            if (c.Trouble_Code__c.Contains('Tow') && c.Miscellaneous_Call_Reason__c !=null && c.Miscellaneous_Call_Reason__c =='Lost Keys')
                                EmailEHILostKey();
                        }
                        catch(System.exception ex){}
                        
                        try
                        {
                            if(c.Tow_Exchange__c !=null && c.Tow_Exchange__c =='Yes')
                                EmailEHITowExchange();
                        }
                        catch(System.exception ex){}
                    }
                    
                }
                system.debug('Success8');
                page_message = updateCase?'Confirm':'Error';
            } else {
                page_message = 'Warning';
                altMessage = 'This call has already been dispatched. If you would like to dispatch another service call, please CLONE this call and click on the Dispatch Call button.';
            }
        } catch(Exception e) {
            page_message = 'Error';
        }
    }
     
     // MBC Changes END   
}