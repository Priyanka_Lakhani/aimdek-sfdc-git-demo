@isTest (seeAllData =true)
private class JLRMTReportController_Test {
    static Account a1 {get;set;}
    static Account a2 {get;set;}
    static Case c1 {get;set;}
    static Case c2 {get;set;}
    static Case c3 {get;set;}
    static string SelectedDealer {get;set;}
    static string fromdate {get;set;}
    static string todate {get;set;}
    
    static void Utility()
    {
        a1 = new Account(
          name='MT1',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-123',
          SecondAccountNumber__c='R0123',
          Status__c ='Active');
        insert a1;
        a2 = new Account(
          name='MT3',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-333',
          SecondAccountNumber__c='R0333',
          Status__c ='Active');
        insert a2;
        c1 = new Case(       
          Trouble_Code__c='Gas',
          MobileTech__c= a1.id,         
          MTCloseReason__c='GOA',
          MobTechDenied__c='Yes',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABC',
          postal_code__c='K178J8');
        insert c1;
        c2 = new Case(       
          Trouble_Code__c='Install Spare',
          MobileTech__c= a1.id,          
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtyy',
          postal_code__c='K178J9');
        insert c2;
        c3 = new Case(       
          Trouble_Code__c='Tow',
          MobileTech__c= a1.id,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwt',
          postal_code__c='K178J8');
        insert c3;       
    }
    static void FilterUtility(){
        SelectedDealer = 'MT1';
        fromDate= '15/08/2017';
        todate = '31/08/2017';
    }
    
    static testMethod void myUnitTest() {
      Utility();
      
     JLRMTReportController mt1 = new JLRMTReportController();
      mt1.show();
      mt1.updatePage(); 
      
      mt1 = new JLRMTReportController();
      mt1.prevBtn();     
   } 
   static testMethod void myUnitTest1() {
      Utility();
      FilterUtility();
      JLRMTReportController mt2 = new JLRMTReportController();
      mt2.filterReport();
      
   }
   
   static testMethod void myUnitTest2() {
      Utility();
      JLRMTReportController mt1 = new JLRMTReportController();
      mt1.FirstBtn();      
    
      mt1 = new JLRMTReportController();
      mt1.nextBtn();
   }
   
   static testMethod void myUnitTest3() {
      Utility();
      JLRMTReportController mt1 = new JLRMTReportController();
      mt1.lastBtn();     
      
      boolean isNext = mt1.getNext();
      boolean isPrev = mt1.getPrev();
   }
   static testMethod void myUnitTest4() {
      Utility();
      
      JLRMTReportController mt1 = new JLRMTReportController();
      mt1.ExportAll();  
   } 
   /*static testMethod void myUnitTest5() {
      Utility();
      
      JLRMTReportController mt1 = new JLRMTReportController();
      mt1.generateDetailReport(); 
   } 
   static testMethod void myUnitTest6() {
      Utility();
      
      JLRMTReportController mt1 = new JLRMTReportController();
      mt1.generateYearlyReport(); 
   } */
}