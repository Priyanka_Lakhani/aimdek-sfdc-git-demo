public class JSON2ApexMatrix {
	public string Status {get;set;}
    public list<string> destination_addresses  {get;set;}
    public list<string> origin_addresses  {get;set;}
    
    public list<JSON2ApexMatrixRow> Rows  {get;set;}
    
    public class JSON2ApexMatrixRow
    {
    	public list<JSON2ApexMatrixElement> elements  {get;set;}
    }
    
    public class JSON2ApexMatrixElement
    {
    	public distance distance {get;set;}    	
    	public duration duration {get;set;}    	
    }
    
    public class distance
    {
        public string text {get;set;}    	
    	public decimal value {get;set;}    	
    }
    
    public class duration
    {
        public string text {get;set;}    	
    	public decimal value {get;set;}    	
    }
    
}