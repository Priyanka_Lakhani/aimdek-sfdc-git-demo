public with sharing class MTReportController{ 

    public List<Summary> SummaryList {get;set;}
    public List<Account> AcctList;
    public List<Case> caseList;
    public CaseSummary cs;
    public Integer recordPerPage { get; set; }
    public Integer totalRecords { get; set; }

    integer totalRecs = 0;
    integer count= 0;
    integer LimitSize= 10;
    public MTReportController(){
        List<RecordType> listOfRT = [Select id from RecordType where name='Mobile Tech'];
        if(listOfRT.size() > 0){
            ID rid = listOfRT[0].id;
            totalRecords =totalRecs= [SELECT count() FROM Account WHERE Name != null and RecordTypeId =:rid and (NOT Name Like '%test%')];
        }else{
            totalRecords =totalRecs= [SELECT count() FROM Account WHERE Name != null and Type = 'Mobile Tech' and (NOT Name Like '%test%')];            
        }
         
    }
    
    public class CaseSummary{
        public Integer TotalCases {get;set;}
        public Integer AcceptedCases {get;set;}
        public Integer DeclinedCases {get;set;}
        public Integer GOACases {get;set;}
        public Integer AverageETA {get;set;}
        public Decimal Acceptance  {get;set;}
        public Integer AverageATA {get;set;}
        public Decimal MadeETA {get;set;}
    }
    public class Summary {
        
        public String RetailerName {get;set;}
        public String ActiveDate {get;set;}
        public String ProviderCurrentStatus {get;set;}
        public String RetailerCodeJag {get;set;}
        public String RetailerCodeLR {get;set;}
        public Integer EventsOffered {get;set;}
        public Integer EventsAccepted {get;set;}
        public Integer EventsDeclined {get;set;}
        public Integer EventsGOA {get;set;}
        public Decimal AcceptancePer {get;set;}
        public Integer AverageETAMin {get;set;}
        public Integer AverageATAMin {get;set;}
        public Decimal MadeETAPer {get;set;}
        
        public Summary(Account acc,CaseSummary cs) {
            EventsOffered =  cs.TotalCases;
            RetailerName = acc.Name;
            ProviderCurrentStatus = acc.Status__c;
            RetailerCodeJag = acc.account_number__c;
            RetailerCodeLR = acc.SecondAccountNumber__c;
            DateTime dT = acc.createdDate;           
            ActiveDate = dT.format('MM/dd/yyyy') ;
            EventsAccepted = cs.AcceptedCases;
            EventsDeclined = cs.DeclinedCases;
            EventsGOA = cs.GOACases;
            AcceptancePer = cs.Acceptance;
            AverageETAMin = cs.AverageETA;
            AverageATAMin = cs.AverageATA;
            MadeETAPer = cs.MadeETA;
        }
    }
    public void exportToExcel(){
        try{
            SummaryList = new List<Summary>();
            AcctList = new List<Account>();           
            List<RecordType> listOfRT = [Select id from RecordType where name='Mobile Tech'];
            if(listOfRT.size() > 0){
                ID rid = listOfRT[0].id;
                AcctList = [SELECT Id,Name,createdDate,Account_Number__c,SecondAccountNumber__c,Status__c FROM Account WHERE Name != null and RecordTypeId =:rid and (NOT Name Like '%test%')];
            }else{
                AcctList = [SELECT Id,Name,createdDate,Account_Number__c,SecondAccountNumber__c,Status__c FROM Account WHERE Name != null and Type = 'Mobile Tech' and (NOT Name Like '%test%') ];            
            }
            for(Account a : AcctList) {
                system.debug(a.Name);
                caseList = new List<Case>(); 
               
                caseList = [select casenumber, MTCloseReason__c, MobTechDenied__c,ETA__c,Arrival_Minutes__c from case where Trouble_Code__c != 'Tow' and mobiletech__r.id =:a.Id and (NOT First_Name__c Like '%test%') and (NOT Last_Name__c Like '%test%') and createdDate >= 2017-07-01T01:02:03Z and createdDate <= 2017-07-31T01:02:03Z];
                
                integer acceptedCases = 0;
                integer declinedCases = 0;
                integer etaCases = 0;
                integer ataCases = 0;
                integer GOACases = 0;
                integer TotalETA = 0;
                Decimal TotalATA = 0;
                for(Case caseObj : caseList){
                    if(caseObj.MobTechDenied__c == 'Yes'){
                        declinedCases +=1;
                    }else{
                        acceptedCases +=1;
                        if(caseObj.ETA__c != null && caseObj.ETA__c !=0){
                            TotalETA += (Integer)caseObj.ETA__c;
                            etaCases +=1;
                        }                        
                        if(caseObj.Arrival_Minutes__c != null && caseObj.Arrival_Minutes__c != 0){
                            TotalATA += (Integer)caseObj.Arrival_Minutes__c;
                            ataCases +=1;
                        }
                    }                    
                    if(caseObj.MTCloseReason__c == 'GOA')
                        GOACases +=1;                    
                }                                                
                cs = new CaseSummary();
                cs.TotalCases = acceptedCases+declinedCases;
                cs.AcceptedCases = acceptedCases;
                cs.DeclinedCases = declinedCases;
                cs.GOACases = GOACases;
                if(etaCases !=0){
                    cs.AverageETA = (Integer)TotalETA / etaCases;                     
                }
                if(ataCases != 0){
                    Integer avgATA =(Integer) TotalATA / ataCases;
                    if(avgATA  != 0)
                        cs.AverageATA = avgATA;
                }
                integer madeETACount = 0;
                for(Case caseObj : caseList){
                    if(caseObj.MobTechDenied__c != 'Yes' && caseObj.Arrival_minutes__c != null && caseObj.Arrival_minutes__c != 0){                     
                        if(caseObj.Arrival_minutes__c <= cs.AverageETA)
                            madeETACount +=1;
                    }                                                            
                }
                if(ataCases != 0){
                    Decimal mdETA = (Decimal)(madeETACount/(Decimal)(ataCases))*100;
                    cs.MadeETA = mdETA.setScale(2);
                }else{
                    if(cs.AverageETA != null)
                        cs.MadeETA =  0.00;
                }
                if((acceptedCases+declinedCases)!=0){              
                    Decimal avg = (Decimal) (acceptedCases/(Decimal)((acceptedCases+declinedCases)))*100;
                    system.debug('per:'+avg.setScale(2));
                    cs.Acceptance = avg.setScale(2) ;
                }else{
                    cs.Acceptance = 0.00;
                }                                   
                SummaryList.add(new Summary(a,cs));                
            }    
        }catch(System.Exception ex){
            system.debug(ex);
        }
    }
    
    public void show() {
         try{              
            SummaryList = new List<Summary>();
            AcctList = new List<Account>();           
            List<RecordType> listOfRT = [Select id from RecordType where name='Mobile Tech'];
            if(listOfRT.size() > 0){
                ID rid = listOfRT[0].id;
                AcctList = [SELECT Id,Name,createdDate,Account_Number__c,SecondAccountNumber__c,Status__c FROM Account WHERE Name != null and RecordTypeId =:rid and (NOT Name Like '%test%') LIMIT:limitsize OFFSET:count];
            }else{
                AcctList = [SELECT Id,Name,createdDate,Account_Number__c,SecondAccountNumber__c,Status__c FROM Account WHERE Name != null and Type = 'Mobile Tech' and (NOT Name Like '%test%') LIMIT:limitsize OFFSET:count];            
            }
            for(Account a : AcctList) {
                
                caseList = new List<Case>(); 
               
                caseList = [select casenumber, MTCloseReason__c, MobTechDenied__c,ETA__c,Arrival_Minutes__c from case where Trouble_Code__c != 'Tow' and mobiletech__r.id =:a.Id and (NOT First_Name__c Like '%test%') and (NOT Last_Name__c Like '%test%') and createdDate >= 2017-07-01T01:02:03Z and createdDate <= 2017-07-31T01:02:03Z ];
                
                integer acceptedCases = 0;
                integer declinedCases = 0;
                integer etaCases = 0;
                integer ataCases = 0;
                integer GOACases = 0;
                integer TotalETA = 0;
                Decimal TotalATA = 0;
                for(Case caseObj : caseList){
                    if(caseObj.MobTechDenied__c == 'Yes'){
                        declinedCases +=1;
                    }else{
                        acceptedCases +=1;
                        if(caseObj.ETA__c != null && caseObj.ETA__c !=0){
                            TotalETA += (Integer)caseObj.ETA__c;
                            etaCases +=1;
                        }                        
                        if(caseObj.Arrival_Minutes__c != null && caseObj.Arrival_Minutes__c != 0){
                            TotalATA += (Integer)caseObj.Arrival_Minutes__c;
                            ataCases +=1;
                        }
                    }                    
                    if(caseObj.MTCloseReason__c == 'GOA')
                        GOACases +=1;                    
                }                                                
                cs = new CaseSummary();
                cs.TotalCases = acceptedCases+declinedCases;
                cs.AcceptedCases = acceptedCases;
                cs.DeclinedCases = declinedCases;
                cs.GOACases = GOACases;
                if(etaCases !=0){
                    cs.AverageETA =(Integer) TotalETA / etaCases;                     
                }
                if(ataCases != 0){
                    Integer avgATA =(Integer) TotalATA / ataCases;
                    if(avgATA  != 0)
                        cs.AverageATA = avgATA;
                }
                integer madeETACount = 0;
                for(Case caseObj : caseList){
                    if(caseObj.MobTechDenied__c != 'Yes' && caseObj.Arrival_minutes__c != null && caseObj.Arrival_minutes__c != 0){                     
                        if(caseObj.Arrival_minutes__c <= cs.AverageETA)
                            madeETACount +=1;
                    }                                                            
                }
                if(ataCases != 0){
                    Decimal mdETA = (Decimal)(madeETACount/(Decimal)(ataCases))*100;
                    cs.MadeETA = mdETA.setScale(2);
                }else{
                    if(cs.AverageETA != null)
                        cs.MadeETA =  0.00;
                }
                if((acceptedCases+declinedCases)!=0){              
                    Decimal avg = (Decimal) (acceptedCases/(Decimal)((acceptedCases+declinedCases)))*100;
                    system.debug('per:'+avg.setScale(2));
                    cs.Acceptance = avg.setScale(2) ;
                }else{
                    cs.Acceptance = 0.00;
                }                                  
                SummaryList.add(new Summary(a,cs));                
            }    
        }catch(System.Exception ex){
            system.debug(ex);
        }
    }
    
    public void updatePage() {
       SummaryList.clear();
       limitsize=recordPerPage;
       show();
    }
    public PageReference Firstbtn() {
        count=0;
        show();
        return null;
    }
    public PageReference prevbtn() {
        count=count-limitsize;
        show();
        return null;
    }
   
    public PageReference Nextbtn() {
        count=count+limitsize;
        show();
        return null;
    }
    public PageReference lastbtn() {
        count= totalrecs - math.mod(totalRecs,LimitSize);
        show();
        return null;
    }


    public Boolean getNext() {
        if((count+ LimitSize) >= totalRecs)
       return true;
     else
       return false;
        
    }
    public Boolean getPrev() {
      if(count== 0)
          return true;
        else
          return false;     
    }
    
}