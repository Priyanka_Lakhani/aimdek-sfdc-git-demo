@isTest (seeAllData =true)
private class MTReportController_Test {
    
    static testMethod void myUnitTest() {
        
      Account a1 = new Account(
          name='MT1',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-123',
          SecondAccountNumber__c='R0123',
          Status__c ='Active');
      insert a1;
                  
      Case c1 = new Case(       
          Trouble_Code__c='Gas',        
          MTCloseReason__c='GOA',
          MobTechDenied__c='Yes',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABC');
      insert c1;
     
      Case c2 = new Case(       
          Trouble_Code__c='Install Spare',
          MobileTech__c= a1.id,          
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtyy');
      insert c2;
      
      Case c4 = new Case(       
          Trouble_Code__c='Tow',
          MobileTech__c= a1.id,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwt');
      insert c4;
      
      MTReportController mt1 = new MTReportController();
      mt1.show();
     
    }
    static testMethod void myUnitTest2() {  
        Account a3 = new Account(
          name='MT3',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-333',
          SecondAccountNumber__c='R0333',
          Status__c ='Active');
      insert a3;
      
       MTReportController mt1 = new MTReportController();
       mt1.prevBtn();
        
      Case c10 = new Case(       
          Trouble_Code__c='Gas',        
          MTCloseReason__c='GOA',
          MobTechDenied__c='Yes',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='AAATT');
      insert c10;
      mt1 = new MTReportController();
      mt1.FirstBtn();      
   }
      static testMethod void myUnitTest3() {  
        Account a3 = new Account(
          name='MT3',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-333',
          SecondAccountNumber__c='R0333',
          Status__c ='Active');
      insert a3;
      
       MTReportController mt1 = new MTReportController();
       mt1.exportToExcel();
        
      Case c10 = new Case(       
          Trouble_Code__c='Gas',        
          MTCloseReason__c='GOA',
          MobTechDenied__c='Yes',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='AAATT');
      insert c10;
      mt1 = new MTReportController();
      mt1.exportToExcel();     
   } 

     static testMethod void myUnitTest4() {  
        Account a3 = new Account(
          name='MT3',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-333',
          SecondAccountNumber__c='R0333',
          Status__c ='Active');
      insert a3;
      
       MTReportController mt1 = new MTReportController();
       mt1.nextBtn();
        
      Case c10 = new Case(       
          Trouble_Code__c='Gas',        
          MTCloseReason__c='GOA',
          MobTechDenied__c='Yes',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='AAATT');
      insert c10;
      mt1 = new MTReportController();
      mt1.lastBtn();     
   } 
}