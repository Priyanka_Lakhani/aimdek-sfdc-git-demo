@RestResource(urlMapping='/MobileIntegrationService/*')
global class MobileIntegrationService {
    @HttpPost 
    global static void go(String method, String LanguageCode, String caseNumber, String name, String email, String comment, String VIN, String mileageAmt, String csiId, String csID,
                          String clientCode, String latitude, String longitude, String xMin, String xMax, String yMin, String yMax, String maxPOICount, String sortBy, 
                          RequestRoadsideAssistanceClass RequestRoadsideAssistance, String CaseID, String CaseUniqueID, String city, String province, String streetname, String crossStreet,    
                          String locationCode, String meetingLocation) {
        if(!Test.isRunningTest()) { 
            RestContext.response.addHeader('Content-Type', 'application/json');
        }
        if(method == 'LocationService') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getLocation(CaseID, CaseUniqueID, city, province, streetname, crossStreet, locationCode, meetingLocation));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}
        }
        if(method == 'getFAQ') { 
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getFAQ(LanguageCode));  
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}        
        }
        if(method == 'getCaseStatus') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getCaseStatus(caseNumber));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}
        }
        if(method == 'setFeedback') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.setFeedback(name, email, comment));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}
        }  
        if(method == 'getProvince') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getProvince());
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}
        }             
        if(method == 'getProblemType') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getProblemType(LanguageCode));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}
        }    
        if(method == 'getLocationType') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getLocationType(LanguageCode));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}            
        }   
        if(method == 'ValidateVIN') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.ValidateVIN(VIN));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}        
        } 
        if(method == 'VINWarrantyCheck') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.VINWarrantyCheck(VIN,mileageAmt));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}            
        }       
        if(method == 'findClubAutoDealer') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.findClubAutoDealer(clientCode, latitude, longitude, xMin, xMax, yMin, yMax, maxPOICount, sortBy));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}            
        }  
        if(method == 'CoverageSummary') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.CoverageSummary(VIN));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}            
        }     
        if(method == 'cancelRoadsideRequest') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.cancelRoadsideRequest(csiId));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}            
        }   
        if(method == 'getServiceTrackerData') {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.getServiceTrackerData(csID));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}            
        }                                      
        if(RequestRoadsideAssistance != null)
        {
            Blob body = Blob.valueOf(MobileIntegrationServiceHelper.requestRoadsideAssistance(RequestRoadsideAssistance));
            if(!Test.isRunningTest()){RestContext.response.responseBody = body;}
        }                
    }    
    global class RequestRoadsideAssistanceClass {
        global String vinLookup {get; set;} 
        global String callbackTypeCode {get; set;} 
        global String breakdownLocationType {get; set;} 
        global String tCode {get; set;} 
        global String latitude {get; set;} 
        global String longitude {get; set;} 
        global String streetName {get; set;} 
        global String programName {get; set;}   
        global String callbackNumber {get; set;}
        global String firstName {get; set;}
        global String lastName {get; set;}
        global String odometer {get; set;}
        global String crossStreet {get; set;}
        global String streetNumber {get; set;}
        global String city {get; set;}
        global String stateCode {get; set;}
        global String postalCode {get; set;}
        global String breakdownLandmark {get; set;}
        global String make {get; set;}
        global String model {get; set;}
        global String color {get; set;}
        global String year {get; set;}
        global String passengerCount {get; set;}
        global String paceSetter {get; set;}
        global String towDestination {get; set;}
        global String towDestinationAddr {get; set;}
        global String towDestinationCity {get; set;}
        global String towDestinationState {get; set;}
        global String TowDestLat {get; set;}
        global String TowDestLng {get; set;}    
        global String towOptions {get; set;}            
    }           
}