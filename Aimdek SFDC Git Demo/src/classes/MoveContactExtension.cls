public with sharing class MoveContactExtension {

    Id currentContactId;
    Contact currentContact;
    Contact newContact;
    List<Employment_History__c> previousEmployment;
    public MoveContactExtension(ApexPages.StandardController controller) {
        try {
    
            currentContactId = controller.getRecord().Id;
            currentContact = [Select Id, Name, Salutation, FirstName, LastName, HomePhone, Birthdate, Phone, Title, MobilePhone, Email, Description, AccountId from Contact where Id =: currentContactId limit 1];
        }
        catch(Exception e) {
            System.debug('Failed to Initialize: ' + e);
        }
        System.debug('currentContactId: ' + currentContactId);
        System.debug('currentContact: ' + currentContact);
    }

    public Contact getContactToMove() {
        if(newContact == null) {
            try {
                newContact = new Contact();
                newContact.Salutation=currentContact.Salutation;
                newContact.FirstName=currentContact.FirstName;
                newContact.LastName=currentContact.LastName;
                newContact.HomePhone = currentContact.HomePhone;
                newContact.Birthdate = currentContact.Birthdate;
                System.debug('NewContact: ' + newContact);
                System.debug('CurrentContact: ' + currentContact);  
            }
            catch(Exception e) {}       
        }
        return this.newContact;
    }   
    
    public void setContactToMove(Contact c) {
        this.newContact = c;
    }       

    public PageReference MoveContact() {
        
        List<Employment_History__c> empHistory = new List<Employment_History__c>();
        if(newContact != null) {
            
            List<Contact> current = new List<Contact>();
            String newLastName = currentContact.LastName+ ' (Historical)';
            System.debug('New Contact: ' + newContact);
            try {
                current.add(new Contact(Id = currentContactId, LastName = newLastName, isHistorical__c=true, HasOptedOutOfEmail=true, DoNotCall=true, Status__c = 'Inactive'));
                //update historical contact's name
                Database.update(current);
                //insert new contact
                Database.insert(newContact);
                
                //bring over any previous employment history
                previousEmployment = [select Id, Move_Date__c, Notes__c, Current_Contact__c, Historical_Account__c, Historical_Contact__c from Employment_History__c where Current_Contact__c =: currentContactId AND isDeleted=false limit:Limits.getLimitDmlRows()];
                if(previousEmployment != null) {
                    
                    Employment_History__c temp;
                    for(Employment_History__c eh : previousEmployment) {
                        
                        eh.Current_Contact__c = newContact.Id;
                        empHistory.add(eh);
                    }
                    try {
                        Database.update(empHistory);
                    }
                    catch(Exception e) {
                        System.debug('Failed to update history items: ' + e);
                    }
                }
                
                //create newest employment history item
                Employment_History__c emp = new Employment_History__c();
                emp.Current_Contact__c = newContact.Id;
                emp.Historical_Account__c = currentContact.AccountId;
                emp.Historical_Contact__c = currentContact.Id;
                emp.Move_Date__c = newContact.Last_Move_Date__c;
                emp.Notes__c = newContact.Move_Notes__c;
                
                Database.insert(emp);
                System.debug('History: ' + empHistory);
            }
            catch(Exception e) {
                System.debug('Insert Failed: ' + e);
            }
        }
        PageReference ref = new PageReference('/' + newContact.Id);
        ref.setRedirect(true);
        return ref; 
        
    }
        
    public PageReference Cancel() {
        
        PageReference ref = new PageReference('/' + currentContactId);
        ref.setRedirect(true);
        return ref; 
        
    }   
       
    static testMethod void testTaskInsert() 
    {
        Account a = new Account(Name='test account');
        Database.insert(a);
        Contact c = new Contact(LastName='Jones', AccountId=a.id);
        Database.insert(c);
        
        Employment_History__c emp = new Employment_History__c();
        emp.Current_Contact__c = c.Id;
        emp.Historical_Account__c = a.Id;
        emp.Historical_Contact__c = c.Id;
        emp.Move_Date__c = System.Today();
        emp.Notes__c = 'Moving Notes!!!';
        Database.insert(emp);
                
        PageReference testPage = new PageReference('/apex/MoveContact?Id='+c.Id);
        Test.setCurrentPage(testPage);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(c);
        MoveContactExtension mce = new MoveContactExtension(controller);
        
        mce.getContactToMove();
        mce.MoveContact();
        mce.Cancel();
        mce.setContactToMove(c);
        
    }       
}