public class PromoController {
 
    public virtual class BaseRequest
    {
        public string  Type {get;set;}
        public string Program {get;set;}
        public string Token {get;set;}
    }

    public class SearchRequest extends BaseRequest
    {
        public string Keyword {get;set;}
    }
        
    public class SubmitRequest extends BaseRequest
    {
        public string VIN {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Phone {get;set;}
        public string Email {get;set;}
        public date PromoStartDate {get;set;}
        public date PromoEndDate {get;set;}
        public decimal MaxOdo {get;set;}
        public string Reason {get;set;}
        public decimal Cost {get;set;}
        public string Language {get;set;}
        public string Street {get;set;}
        public string City {get;set;}
        public string Province {get;set;}
        public string PostalCode {get;set;}
        public decimal CurrentOdo {get;set;}
    }
    
    public class LoginRequest extends BaseRequest
    {
        public string UserName {get;set;}
        public string Password {get;set;}
    }
 
    public class ResetPwdRequest extends BaseRequest
    {
        public string UserName {get;set;}
        public string OldPassword {get;set;}
        public string NewPassword {get;set;}
    }

    public class VINRequest extends BaseRequest
    {
        public string VIN {get;set;}
        public integer CurrentOdo {get;set;}
    }
    /* Forgot Password and Username Changes : Start */
    public class TokenForForgotPwdRequest extends BaseRequest
    {
        public string Email {get;set;}
    }
    
    public class ForgotPwdRequest extends BaseRequest
    {
        public string UserName {get;set;}
        public string NewPassword {get;set;}
        public string Token {get;set;}
    }
    
    public class ForgotUserNameRequest extends BaseRequest
    {
        public string Email {get;set;}
    }
    /* Forgot Password and Username Changes : End */
    
    public class VINInfo
    {
        public string VIN {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string Phone {get;set;}
        public string Email {get;set;}
        public string Street {get;set;}
        public string City {get;set;}
        public string State {get;set;}
        public string Country {get;set;}
        public string PostalCode {get;set;}
        public Boolean  WarrantyExpired {get;set;}
        public string Status {get;set;}
        public list<PromoRecord>  History {get;set;}
        public CurrentWarranty CurrentWarranty {get;set;}
    }

    public class CurrentWarranty
    {
         public string BaseStartDate {get;set;}
         public string BaseEndDate {get;set;}
         public string BaseMaxOdo {get;set;}
         public string ExtendedStartDate {get;set;}      //could be blank
         public string ExtendedEndDate {get;set;}        //could be blank
         public string ExtendedMaxOdo {get;set;}       //could be blank
         public string PromoStartDate {get;set;}    //could be blank
         public string PromoEndDate {get;set;}    //could be blank
         public string PromoMaxOdo {get;set;}      //could be blank. If promo start/end date are blank, promomaxod blank means unlimited warranty.
    }

    public class PromoRecord
    {
         public string FirstName {get;set;}
         public string LastName {get;set;}
         public string Phone {get;set;}
         public string Email {get;set;}
         public Date PromoStartDate {get;set;}
         public Date PromoEndDate {get;set;}
         public decimal CurrentOdo {get;set;}
         public decimal MaxOdo {get;set;}
         public Date SubmissionDate {get;set;}
         public string Dealership {get;set;}
         public string VIN {get;set;}
         public decimal cost {get;set;}
         public string year {get;set;}
         public string model {get;set;}
         public string make {get;set;} 
         public string reason {get;set;}
    }

    public class VINRequestSuccess  extends Success
    {
        //public string status {get;set;}
        public VINInfo VINInfo {get;set;}
    }


    public virtual class Success 
    {
        public String Status {get; set;}
        public Success()
        {
            Status = 'Success';
        }
    }   
    public class LoginSuccess extends Success 
    {
        public string Token {get;set;}
        public boolean Reset {get;set;}
        public string DealerName {get;set;}
        public string DealerPhone {get;set;}
        public string DealerAddress {get;set;}
    }
    
    public class HistorySuccess extends Success 
    {
        public string DealerName {get;set;}
        public list<PromoRecord> History {get;set;}
    }
    
    /* Forgot Password and Username Changes : Start */
    
    public class TokenForForgotPwdSuccess extends Success
    {
        public string DealerId {get;set;}
        public string Token {get;set;} 
    }
    
    public class ForgotUserNameSuccess extends Success
    {
        public string DealerId {get;set;}
        public string Email {get;set;} 
    }

    /* Forgot Password and Username Changes : End */
    
     public class Error {
        public String Status {get; set;}
        public string Error {get;set;}
        public Error()
        {
            Status = 'Error';
        }
    }   
    
    public string Response{get;set;}
    string jstring;
    //string token ;
    string program ;
    id dealerID ;   
    Error error;
    string clientcode;
    string promoclientcode;
        
    
    public void init() {ProcessRequest(); }
    
    private void ProcessRequest()
    {
        error = new error();
        String apiName = '';
        jstring = '';
        //token = '';
        program ='';
        clientcode ='';
        
        Map<String, String>  headers = ApexPages.currentPage().getHeaders();
        //if (headers.get('Content-Type') == 'application/x-www-form-urlencoded')
        //{
            //if (ApexPages.currentPage().getParameters().get('Program') != null && ApexPages.currentPage().getParameters().get('Program') != '')
            //program = ApexPages.currentPage().getParameters().get('Program');
             
            //if (ApexPages.currentPage().getParameters().get('Token') != null && ApexPages.currentPage().getParameters().get('Token') != '')
            //token = ApexPages.currentPage().getParameters().get('Token');
            
            if (ApexPages.currentPage().getParameters().get('Promo') != null && ApexPages.currentPage().getParameters().get('Promo') != '')
            jstring = ApexPages.currentPage().getParameters().get('Promo');
            
            
            system.debug('jString:' + jstring);
            Integration__c debug = new Integration__c(IP__c=headers.get('X-Salesforce-SIP'));
            debug.Parameters__c = jstring;
            debug.Message__c = 'AudiPromo';
            
            upsert debug;
            
            if (jstring.trim() =='')
            {   
                error.Error ='Request Content is Blank';
                Response = JSON.serializePretty(error);
                return;             
            }

            BaseRequest req = (BaseRequest)JSON.deserialize(jstring,BaseRequest.class);
            apiName = req.Type;
            program = req.Program;

        //}
        //else
        //{
        //  error.Error ='Unkown Header';
        //  Response = JSON.serializePretty(error);         
        //  return;
        //} 
        /* Forgot Password and Username Changes */
        if (apiName !='GetVINInfo' && apiName !='SubmitRequest' && apiName !='Login' && apiName != 'ResetPassword' && apiName != 'GetAllHistory'&& apiName != 'SearchUser' && apiName != 'GetTokenForForgotPassword' && apiName != 'UpdatePassword' && apiName != 'ForgotUserName')
        {
            system.debug('Unknown API: ' + apiName);
            error.Error ='Unknown API: ' + apiName;
            Response = JSON.serializePretty(error);         
            return;
        }
        
        if (Program != 'Audi')
        {
            error.Error ='Unknown Program ' + Program;
            Response = JSON.serializePretty(error);
            return;
        }
        
        if (Program == 'Audi')
        {
            clientcode = '511';
            promoclientcode = '541';
        }
        if (apiName == 'GetVINInfo')
            GetVINInfo();
        if (apiName == 'SubmitRequest')
            SubmitRequest();
        if (apiName == 'Login')
            Login();
        if (apiName == 'ResetPassword')
            ResetPassword();    
        if (apiName == 'GetAllHistory')
            GetAllHistory();    
        if (apiName == 'SearchUser')
            SearchUser();
        /* Forgot Password and Username Changes : Start */
        if (apiName == 'GetTokenForForgotPassword')
            GetTokenForForgotPassword();
        if (apiName == 'UpdatePassword') 
            UpdatePassword();  
        if (apiName == 'ForgotUserName')
            ForgotUserName();    
        /* Forgot Password and Username Changes : End */
    }
    
    
    public void GetVINInfo()
    {
        VINRequest req = (VINRequest)JSON.deserialize(jstring,VINRequest.class);
        integer currentodo = req.currentOdo;
    
        Account a;
        VIN__c v;
        try 
        {
            a = [Select Id, Name, WEBLOGINCODE__C,ResetWebCode__c,token__c from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
            
            try
            {
                 v = [Select Id, status__c,name,first_name__c,last_name__c,phone__c,email__c,street__c,city__c,state__c,country__c,postal_code__c,Promo_End__c,Extended_Warranty_End__c,Base_Warranty_End__c,Promo_Start__c,Extended_Warranty_Start__c,Base_Warranty_Start__c,Base_Warranty_Max_Odometer__c,Extended_Warranty_Max_Odometer__c,Promo_Max_Odometer__c from vin__c where program__r.program_code__c =:clientcode and name = :req.vin limit 1];
            }
            catch(System.exception ex1)
            {
                error.error = 'VIN not Found under this program';
                Response = JSON.serializePretty(error);
                return;
            }
            
            boolean expired = true;
            
            boolean baseexp = true;
            boolean extexp = true;
            boolean promoexp = true;
            
            if (v.Base_Warranty_End__c != null && v.Base_Warranty_End__c > system.today() && (v.Base_Warranty_Max_Odometer__c != null) && currentodo < v.Base_Warranty_Max_Odometer__c)
                baseexp = false;

            if (v.Extended_Warranty_End__c != null && v.Extended_Warranty_End__c > system.today() && (v.Extended_Warranty_Max_Odometer__c != null) && currentodo < v.Extended_Warranty_Max_Odometer__c)
                extexp = false;
                
            if (v.Promo_End__c != null && v.Promo_End__c > system.today() && (v.Promo_Max_Odometer__c != null) && currentodo < v.Promo_Max_Odometer__c)
                promoexp = false;
            
            expired = baseexp && extexp && promoexp;
                        
            VINInfo vinfo = new VINInfo();
            vinfo.VIN = v.name;
            vinfo.Status = v.Status__c;
            vinfo.firstname = v.first_name__c;
            vinfo.lastname = v.last_name__c;
            vinfo.phone = v.phone__c;
            vinfo.email = v.email__c;
            vinfo.street = v.street__c;
            vinfo.city = v.city__c;
            vinfo.State=v.state__c;
            vinfo.country=v.country__c;
            vinfo.postalcode = v.postal_code__c;
            vinfo.WarrantyExpired = expired;
            
            CurrentWarranty cw = new CurrentWarranty();
            cw.BaseStartDate = string.valueof(v.Base_Warranty_Start__c);
            cw.BaseEndDate = string.valueof(v.Base_Warranty_End__c);
            cw.BaseMaxOdo = string.valueof(v.Base_Warranty_Max_Odometer__c);
            cw.ExtendedStartDate = string.valueof(v.Extended_Warranty_Start__c);
            cw.ExtendedEndDate = string.valueof(v.Extended_Warranty_End__c);
            cw.ExtendedMaxOdo = string.valueof(v.Extended_Warranty_Max_Odometer__c);
            cw.PromoStartDate = string.valueof(v.Promo_Start__c);
            cw.PromoEndDate = string.valueof(v.Promo_End__c);
            cw.PromoMaxOdo = string.valueof(v.Promo_Max_Odometer__c);
            
            vinfo.CurrentWarranty = cw;
            
            list<PromoRecord> history = new list<PromoRecord>();
            

            list<Promo__c> ps = [select promostartdate__c,promoenddate__c,maxodo__c,firstname__c,lastname__c,phone__c,email__c,createddate,dealer__r.name from promo__c where vin__r.name = :req.vin order by createddate desc limit 100];
            
            for (promo__c p: ps)
            {
                PromoRecord pr = new PromoRecord();
                pr.firstname = p.firstname__c;
                pr.lastname = p.lastname__c;
                pr.phone = p.phone__c;
                pr.email = p.email__c;
                pr.PromoStartDate = p.promostartdate__c;
                pr.PromoEndDate = p.promoenddate__c;
                pr.MaxOdo = p.maxodo__c;
                pr.SubmissionDate  = Date.valueof( p.createddate);
                pr.dealership = p.dealer__r.name;
                history.add(pr);
            }                           
                        
            vinfo.History = history;

            VINRequestSuccess vreq = new VINRequestSuccess();
            vreq.VINInfo = vinfo;
            Response = JSON.serializePretty(vreq);
            
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name/Password Does not Match our Record';
            Response = JSON.serializePretty(error);
            return;
        }
                        

    }
    public void SubmitRequest()
    {
        
        SubmitRequest req = (SubmitRequest)JSON.deserialize(jstring,SubmitRequest.class);
        
    
        Account a;
        VIN__c v;
        try 
        {
            a = [Select Id, Name, WEBLOGINCODE__C,ResetWebCode__c,token__c from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
            
            try
            {
                 v = [Select Id, name,Promo_Program__c,Promo_Max_Odometer__c, Promo_Start__c, Promo_End__c from vin__c where program__r.program_code__c =:clientcode and name = :req.vin limit 1];
            }
            catch(System.exception ex1)
            {
                error.error = 'VIN not Found under this program';
                Response = JSON.serializePretty(error);
                return;
            }
                            
            Promo__c promo = new promo__c();
            promo.FirstName__c = req.firstname;
            promo.LastName__c = req.LastName;
            promo.Phone__c = req.Phone;
            promo.Email__c = req.Email;
            promo.Reason__c = req.Reason;
            promo.Cost__c = req.cost;
            promo.PromoStartDate__c = req.PromoStartDate;
            promo.PromoEndDate__c = req.PromoEndDate;
            Promo.Dealer__c = a.id;
            Promo.VIN__c = v.id;
            Promo.MaxOdo__c = req.MaxOdo;
            promo.Language__c= req.Language;
            promo.street__c = req.street;
            promo.city__c = req.city;
            promo.province__c = req.province;
            promo.postalcode__c = req.postalcode;
            promo.currentOdo__c = req.CurrentOdo;
            
            insert promo;
            
            
            program__c p = [select id from program__c where program_code__c = :promoclientcode];
            
            v.Promo_Program__c = p.id;
            v.Promo_Start__c = req.PromoStartDate;
            v.Promo_End__c = req.PromoEnddate;
            v.Promo_Max_Odometer__c = req.maxodo;
            
            upsert v;
                        
            Response = JSON.serializePretty(new Success());
            
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name/Password Does not Match our Record';
            Response = JSON.serializePretty(error);
            return;
        }
                        

    }
    public void ResetPassword()
    {
        ResetPwdRequest req = (ResetPwdRequest)JSON.deserialize(jstring,ResetPwdRequest.class);
        string aname = clientcode +'-' + req.UserName;
        
        Account a;
        try 
        {
            try
            {
                a = [Select Id, Name, WEBLOGINCODE__C,ResetWebCode__c,token__c from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'
                    And Account_Number__c = :aname AND WEBLOGINCODE__C =:req.oldpassword and token__c = :req.token limit 1];
            }
            catch(System.exception ex)
            {
                a = [Select Id, Name, WEBLOGINCODE__C,ResetWebCode__c,token__c from Account WHERE Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
                
            }   
            //Success success = new Success();
            
            a.ResetWebCode__c = false;
            a.WEBLOGINCODE__C = req.newpassword;
            update a;
            
            Response = JSON.serializePretty(new Success());
            
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name/Password Does not Match our Record';
            Response = JSON.serializePretty(error);
            return;
        }
                        
    }
    public void Login()
    {
        LoginRequest req = (LoginRequest)JSON.deserialize(jstring,LoginRequest.class);
        string aname = clientcode +'-' + req.UserName;
        Account a;
        try 
        {
            if (req.UserName == '5419999')
                a = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Phone,ResetWebCode__c,token__c from Account WHERE Account_Number__c=:clientCode AND Status__c='Active'
                    AND WEBLOGINCODE__C =:req.password limit 1];
            else
                a = [Select Id, Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Phone,ResetWebCode__c,token__c from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'
                    And Account_Number__c = :aname AND WEBLOGINCODE__C =:req.password limit 1];
                
            LoginSuccess success = new LoginSuccess();
            success.reset = a.resetwebcode__c;
            success.DealerName = a.name;
            success.DealerPhone = a.phone;
            success.DealerAddress = a.BillingStreet + ',' + a.billingCity +',' + a.billingState + ',' + a.billingPostalCode;
            success.Token = GlobalClass.NewGUID();
            Response = JSON.serializePretty(success);
            
            a.token__c = success.Token;
            update a;
            
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name/Password Does not Match our Record';
            Response = JSON.serializePretty(error + '.' + ex.getmessage());
            return;
        }
                        
    }
    
    public void GetAllHistory()
    {
        BaseRequest req = (BaseRequest)JSON.deserialize(jstring,BaseRequest.class);
    
        Account a;
        VIN__c v;
        list<promo__c> promos;
        boolean headoffice = false;
        list<id> accids = new list<id>();
        
        try 
        {
            try
            {
                a = [Select Id, Name from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
            }
            catch(System.Exception ex)
            {
                a = [Select Id, Name from Account WHERE Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
                list<Account> accs = [select id from Account where Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'];
                
                for (Account acc :accs)
                    accids.add(acc.id);
                    
                headoffice = true;
            }
            
            try
            {
                if (headoffice)
                    promos = [Select Id, vin__r.name,firstname__c,lastname__c,phone__c,email__c,PromoStartDate__c,PromoEndDate__c,CreatedDate,currentodo__c,cost__c,vin__r.year__c,vin__r.make__c,vin__r.model__c,dealer__r.name,reason__c from promo__c where dealer__c in :accids order by createddate desc limit 2000];
                else
                    promos = [Select Id, vin__r.name,firstname__c,lastname__c,phone__c,email__c,PromoStartDate__c,PromoEndDate__c,CreatedDate,currentodo__c,cost__c,vin__r.year__c,vin__r.make__c,vin__r.model__c,dealer__r.name,reason__c from promo__c where dealer__c = :a.id order by createddate desc limit 2000];
            }
            catch(System.exception ex1)
            {
                error.error = 'No History Found';
                Response = JSON.serializePretty(error);
                return;
            }
            
            HistorySuccess history = new HistorySuccess();
            history.DealerName = a.Name;
            
            list<PromoRecord> historyrec = new list<PromoRecord>();
            
            
            for (promo__c p: promos)
            {
                PromoRecord pr = new PromoRecord();
                pr.firstname = p.firstname__c;
                pr.lastname = p.lastname__c;
                pr.phone = p.phone__c;
                pr.email = p.email__c;
                pr.PromoStartDate = p.promostartdate__c;
                pr.PromoEndDate = p.promoenddate__c;
                pr.SubmissionDate  = Date.valueof( p.createddate);
                pr.VIN = p.vin__r.name;
                pr.year =p.vin__r.year__c;
                pr.model=p.vin__r.model__c;
                pr.make=p.vin__r.make__c;
                pr.CurrentOdo = p.currentOdo__c;
                pr.cost = p.cost__c;
                pr.dealership = p.dealer__r.name;
                pr.reason = p.reason__c;                
                historyrec.add(pr);
            }                           
            history.History = historyrec;
            Response = JSON.serializePretty(history);       
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name/Password Does not Match our Record. clientcode:' + clientcode + ' token:' + req.token;
            Response = JSON.serializePretty(error);
            return;
        }
                        

    }
    
    
    public void SearchUser()
    {
        SearchRequest req = (SearchRequest)JSON.deserialize(jstring,SearchRequest.class);
    
        if (string.isBlank(req.Keyword))
        {
            error.error = 'No search keyword Entered';
            Response = JSON.serializePretty(error);
            return;
        }
        
        Account a;
        VIN__c v;
        integer  i =0;
        list<promo__c> promos;
        boolean headoffice = false;
        list<id> accids = new list<id>();
        
        try 
        {
            try
            {
                a = [Select Id, Name from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
            }
            catch(System.Exception ex)
            {
                a = [Select Id, Name from Account WHERE Account_Number__c=:clientCode AND Status__c='Active' AND token__c = :req.token limit 1];
                list<Account> accs = [select id from Account where Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'];
                
                for (Account acc :accs)
                    accids.add(acc.id);
                    
                headoffice = true;
            }
            
            
            try
            {
                string lname = req.Keyword + '%';
                string fname = req.Keyword + '%';
                
                if (headoffice)
                    promos = [Select Id, vin__r.name,firstname__c,lastname__c,phone__c,email__c,PromoStartDate__c,PromoEndDate__c,CreatedDate,currentodo__c,cost__c,vin__r.year__c,vin__r.make__c,vin__r.model__c,dealer__r.name,reason__c from promo__c where dealer__c in :accids and (lastname__c  like :lname or firstname__c like :fname) order by createddate desc limit 2000];
                else
                    promos = [Select Id,vin__r.name, firstname__c,lastname__c,phone__c,email__c,PromoStartDate__c,PromoEndDate__c,CreatedDate,currentodo__c,cost__c,vin__r.year__c,vin__r.make__c,vin__r.model__c,dealer__r.name,reason__c from promo__c where dealer__c = :a.id and (lastname__c  like :lname or firstname__c like :fname) order by createddate desc limit 2000];

                    //promos = [Select Id, vin__r.name,firstname__c,lastname__c,phone__c,email__c,PromoStartDate__c,PromoEndDate__c,CreatedDate,currentodo__c,cost__c,vin__r.year__c,vin__r.make__c,vin__r.model__c,dealer__r.name from promo__c where dealer__c = :a.id order by createddate desc limit 2000];
                
            }
            catch(System.exception ex1)
            {
                error.error = 'No History Found';
                Response = JSON.serializePretty(error);
                return;
            }
            
            HistorySuccess history = new HistorySuccess();
            history.DealerName = a.Name;
            
            list<PromoRecord> historyrec = new list<PromoRecord>();
            
            
            if (promos != null)
            {
                for (promo__c p: promos)
                {
                    PromoRecord pr = new PromoRecord();
                    pr.firstname = p.firstname__c;
                    pr.lastname = p.lastname__c;
                    pr.phone = p.phone__c;
                    pr.email = p.email__c;
                    pr.PromoStartDate = p.promostartdate__c;
                    pr.PromoEndDate = p.promoenddate__c;
                    pr.SubmissionDate  = Date.valueof( p.createddate);
                    pr.VIN = p.vin__r.name;
                    pr.year =p.vin__r.year__c;
                    pr.model=p.vin__r.model__c;
                    pr.make=p.vin__r.make__c;
                    pr.CurrentOdo = p.currentOdo__c;
                    pr.cost = p.cost__c;
                    pr.dealership = p.dealer__r.name;               
                    pr.reason = p.reason__c;
                    historyrec.add(pr);
                }
            }                           
            history.History = historyrec;
            Response = JSON.serializePretty(history);       
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name/Password Does not Match our Record. clientcode:' + clientcode + ' token:' + req.token + '  i:' + string.valueof(i) + ' promos:' + promos;
            Response = JSON.serializePretty(error);
            return;
        }
    }
    
    /* Forgot Password and Username Changes : Start */
    
    public void GetTokenForForgotPassword(){
        TokenForForgotPwdRequest req = (TokenForForgotPwdRequest)JSON.deserialize(jstring,TokenForForgotPwdRequest.class);
        string email ='%'+ req.Email+'%';

        Account a;
        try 
        {
            a = [Select Id, Name, Account_Number__c, Business_Email__c, WEBLOGINCODE__C,token__c  from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'
                    And Business_Email__c like :email limit 1];
            
            string dealerId = (a.Account_Number__c).split('-')[1];
            TokenForForgotPwdSuccess success = new TokenForForgotPwdSuccess();
            success.DealerId= dealerId;
            if(a.token__c == null){
                success.Token = GlobalClass.NewGUID();                  
                a.token__c = success.Token;
                update a;            
            }else{
                success.Token = a.token__c ;
            }
            Response = JSON.serializePretty(success);
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or Email Does not Match our Record';
            Response = JSON.serializePretty(error);
            return;
        }
    }
    
    public void UpdatePassword(){
        ForgotPwdRequest req = (ForgotPwdRequest)JSON.deserialize(jstring,ForgotPwdRequest.class);
        string aname = clientcode +'-' + req.UserName;
        
        Account a;
        try 
        {
            try
            {
                a = [Select Id, Name, Business_Email__c, WEBLOGINCODE__C,ResetWebCode__c,token__c from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'
                    And Account_Number__c = :aname limit 1];
            }
            catch(System.exception ex)
            {
                a = [Select Id, Name, Business_Email__c, WEBLOGINCODE__C,ResetWebCode__c,token__c from Account WHERE Account_Number__c=:clientCode AND Status__c='Active' limit 1];
                
            }   
            if(a.token__c == req.Token) {                      
                a.ResetWebCode__c = false;
                a.WEBLOGINCODE__C = req.newpassword;
                a.token__c = GlobalClass.NewGUID();
                update a;
                
                Response = JSON.serializePretty(new Success());
            }else{
                error.error = 'Invalid Token';
                Response = JSON.serializePretty(error);
                return;
            }
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or User Name Does not Match our Record';
            Response = JSON.serializePretty(error);
            return;
        }
    }
    
    public void ForgotUserName(){
        ForgotUserNameRequest req = (ForgotUserNameRequest)JSON.deserialize(jstring,ForgotUserNameRequest.class);
        string email ='%'+ req.Email+'%';

        Account a;
        try 
        {
            a = [Select Id, Name, Account_Number__c, Business_Email__c, WEBLOGINCODE__C from Account WHERE Type='Dealership / Tow Destination' AND Client__r.Account_Number__c=:clientCode AND Status__c='Active'
                    And Business_Email__c like :email limit 1];
            
            string dealerId = (a.Account_Number__c).split('-')[1];
            
            ForgotUserNameSuccess success = new ForgotUserNameSuccess();
            success.DealerId= dealerId;
            success.Email = req.Email;
            Response = JSON.serializePretty(success);
        }
        catch(System.exception ex)
        {
            error.error = 'Account not Found or Email Does not Match our Record';
            Response = JSON.serializePretty(error);
            return;
        }
    }
    /* Forgot Password and Username Changes : End */   
    
}