/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ReportBug_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        CA__c settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';
        upsert settings;
        
        Case c = new Case(
            Status='Spotted',
            City__c='Toronto',
            Province__c='Ontario',
            VIN_Member_ID__c='11111',
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c = 'Break Problem',
            Phone__c='123',
            Call_ID__c='ABC'
        );
        insert c;
        
        ApexPages.currentPage().getParameters().put('b', 'test');
        ApexPages.currentPage().getParameters().put('d', 'test');
        ReportBug rbug = new ReportBug(new ApexPages.StandardController(c));
        rbug.SaveBug();
        
    }
}