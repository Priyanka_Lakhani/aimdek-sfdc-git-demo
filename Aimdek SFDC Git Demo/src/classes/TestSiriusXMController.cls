Public class TestSiriusXMController{
    public String result{get;set;}
    
    public Void readDetailData() {
        result = '';
        try {
            String end_point ='https://spi-sta.sxm.clubautoltd.com/honda/ReadDetailData';
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(end_point);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            // Set the body as a JSON object
            req.setBody('{"referenceId":393346,"contactLastName":"Sharma","contactPhoneNumber":"324234234234"}');
            string rtnmsg = '';            
           
            Http http = new Http();
            HTTPResponse res;//
            res = http.send(req);
     
            rtnmsg += res.getbody();
            System.debug('response:'+rtnmsg);
            result = rtnmsg;
        }catch(System.exception ex){  
              result =  ex.getMessage();                                                            
              System.debug(ex.getMessage()); 
              System.debug('Exception:'+ex);                                                      
        }
    }
    
    public Void requestService() {
        result = '';
        try {
            String end_point ='https://spi-sta.sxm.clubautoltd.com/honda/RequestService';
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(end_point);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            // Set the body as a JSON object
            req.setBody('{"referenceId":100003177,"serviceType":"ECALL_MANUAL","country":"USA"}');
            string rtnmsg = '';            
           
            Http http = new Http();
            HTTPResponse res;//
            res = http.send(req);
     
            rtnmsg += res.getbody();
            System.debug('response:'+rtnmsg);
            result = rtnmsg;
        }catch(System.exception ex){             
              result =  ex.getMessage();                                                  
              System.debug(ex.getMessage()); 
              System.debug('Exception:'+ex);                                                      
        }
    }
    public Void RemoveNotify() {
        result = '';
        try {
            String end_point ='https://spi-sta.sxm.clubautoltd.com/honda/RemoveNotify';
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(end_point);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            // Set the body as a JSON object
            req.setBody('{"referenceId":100003177,"requestorApplication":"Application","requestorAgentId":2343242342342}');
            string rtnmsg = '';            
           
            Http http = new Http();
            HTTPResponse res;//
            res = http.send(req);
     
            rtnmsg += res.getbody();
            System.debug('response:'+rtnmsg);
            result = rtnmsg;
        }catch(System.exception ex){  
              result =  ex.getMessage();                                                             
              System.debug(ex.getMessage()); 
              System.debug('Exception:'+ex);                                                      
        }
    }
   
    public Void ReTransmit() {
        result = '';
        try {
            String end_point ='https://spi-sta.sxm.clubautoltd.com/honda/ReTransmit';
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(end_point);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            // Set the body as a JSON object
            req.setBody('{"referenceId":100003177,"maximumLocationCount":10}');
            string rtnmsg = '';            
           
            Http http = new Http();
            HTTPResponse res;//
            res = http.send(req);
     
            rtnmsg += res.getbody();
            System.debug('response:'+rtnmsg);
            result = rtnmsg;
        }catch(System.exception ex){  
              result =  ex.getMessage();                                                             
              System.debug(ex.getMessage()); 
              System.debug('Exception:'+ex);                                                      
        }
    }
    
    public Void AgentAssigned() {
        result = '';
        try {
            String end_point ='https://spi-sta.sxm.clubautoltd.com/honda/AgentAssigned';
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(end_point);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            // Set the body as a JSON object
            req.setBody('{"referenceId":100003177,"isAssigned":true}');
            string rtnmsg = '';            
           
            Http http = new Http();
            HTTPResponse res;//
            res = http.send(req);
     
            rtnmsg += res.getbody();
            System.debug('response:'+rtnmsg);
            result = rtnmsg;
        }catch(System.exception ex){        
              result =  ex.getMessage();                                                       
              System.debug(ex.getMessage()); 
              System.debug('Exception:'+ex);                                                      
        }
    }
    
    public Void Terminate() {
        result = '';
        try {
            String end_point ='https://spi-sta.sxm.clubautoltd.com/honda/Terminate';//'https://spi-sta.sxm.clubautoltd.com/honda/Terminate';
            HttpRequest req = new HttpRequest();
    
            req.setEndpoint(end_point);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json;charset=UTF-8');
            // Set the body as a JSON object
            req.setBody('{"referenceId":100003177,"reasonCode":"REASON_CODE"}');
            string rtnmsg = '';            
           
            Http http = new Http();
            HTTPResponse res;//
            res = http.send(req);
     
            rtnmsg += res.getbody();
            System.debug('response:'+rtnmsg);
            result = rtnmsg;
        }catch(System.exception ex){      
              result =  ex.getMessage();                                                         
              System.debug(ex.getMessage()); 
              System.debug('Exception:'+ex);                                                      
        }
    }
}