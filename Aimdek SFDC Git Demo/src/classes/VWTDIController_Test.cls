@isTest(SeeAllData=true)
private class VWTDIController_Test {

    static testMethod void myUnitTest() 
    {
    	FormUser__c user = new FormUser__c();
    	user.firstname__c='Test';
    	user.lastname__c='Test';
    	user.loginname__c = 'test';
    	user.password__c ='49';
    	user.reset__c = false;
		insert user;		    	
		
		
		Program__c prog = [select id,account__c,program_code__c from program__c where program_code__c = '511'];
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        VWTDIController tdi = new VWTDIController();
        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.login();
        
        tdi.ResetPwd();
        tdi.newpwd1 = '2';
        tdi.newpwd2 = '1';
        tdi.ChangePwd();

		tdi.pwd = '1';
        tdi.newpwd1 = '1';
        tdi.newpwd2 = '1';
        tdi.loginname = 'teset1';
        tdi.ChangePwd();

        tdi.loginname = 'test';
		tdi.pwd = '1';
        tdi.newpwd1 = '1';
        tdi.newpwd2 = '1';
        tdi.ChangePwd();



		tdi.vin = 'VIN1';
		tdi.checkVIN();
        
		tdi.vin = 'VIN';
		tdi.checkVIN();
		
		tdi.firstname = 'abc';
		tdi.lastname = 'abc';
		tdi.vin = 'vin';
		tdi.phone='4169999999';
		tdi.email='a@a.com';
		tdi.comments = 'aaa';
		tdi.callReason='Other';
		tdi.Submit();
		
		tdi.ChangeLanguage();
		
		//tdi.Logout();
		
		tdi.internalUser = false;
		tdi.CallReason = 'None';
		tdi.Submit();		
		tdi.firstname = 'abc';
		tdi.lastname = 'abc';
		tdi.vin = 'vin';
		tdi.phone='4169999999';
		tdi.email='a@a.com';
		tdi.comments = 'aaa';
		tdi.callReason='Other';
		tdi.Submit();
		
		tdi.Next();
		
		tdi.Logout();
		
    }
    
    
    static testMethod void myUnitTest1() 
    {
    	FormUser__c user = new FormUser__c();
    	user.firstname__c='Test';
    	user.lastname__c='Test';
    	user.loginname__c = 'test';
    	user.password__c ='49';
    	user.reset__c = false;
    	user.whitelist__c = '10.10.1.1';
		insert user;		    	
		
		
		Program__c prog = [select id,account__c,program_code__c from program__c where program_code__c = '511'];
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        PageReference pageRef = Page.VWTDI; //replace with your VF page name
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.1');
        VWTDIController tdi = new VWTDIController();

        tdi.internalUser = false;
        tdi.loginname = 'test1';
        tdi.pwd = '1';
        tdi.login();

        tdi.internalUser = false;
        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.login();

        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.2');
        tdi.internalUser = false;
		tdi.pwd = '1';
        tdi.newpwd1 = '1';
        tdi.newpwd2 = '1';
        tdi.ChangePwd();
        
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.2');
        tdi.internalUser = false;
		tdi.vin = 'VIN';
		tdi.checkVIN();
		
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.2');
        tdi.internalUser = false;
		tdi.firstname = 'abc';
		tdi.lastname = 'abc';
		tdi.vin = 'vin';
		tdi.phone='4169999999';
		tdi.email='a@a.com';
		tdi.comments = 'aaa';
		tdi.callReason='Other';
		tdi.Submit();
		


        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.10.10');
        tdi = new VWTDIController();
        tdi.internalUser = false;
        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.login();

				
    }
    
}