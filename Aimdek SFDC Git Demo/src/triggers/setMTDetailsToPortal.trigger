trigger  setMTDetailsToPortal on Account (after update,after insert) {
    List<String> keyList = new List<String>();
   for(Account a : trigger.new) {
       RecordType AccountRecordType = [SELECT Id FROM RecordType where Name = 'Mobile Tech' LIMIT 1];
       if(a.MTPortalAPICall__c == false && a.RecordTypeId == AccountRecordType.Id ){
           System.debug('***Account****'+a.Name+'######');
           if(Trigger.isUpdate)
            {
                Account gplObject = new Account(); // This takes all available fields from the required object. 
                Schema.SObjectType objType = gplObject.getSObjectType(); 
                Map<String, Schema.SObjectField> mapFields = Schema.SObjectType.Account.fields.getMap(); 
                
                
                for(Account gpl : trigger.new)
                {
                    Account oldGPL = trigger.oldMap.get(gpl.Id);
        
                    for (String str : mapFields.keyset()) 
                    { 
                        try 
                        { 
                            if(gpl.get(str) != oldGPL.get(str))
                            { 
                                System.Debug('IGORU Field changed: ' + str + '. The value has changed from: ' + oldGPL.get(str) + ' to: ' + gpl.get(str)); 
                                keyList.add(str);
                            } 
                        } 
                        catch (Exception e) 
                        { 
                            System.Debug('Error: ' + e); 
                        } 
                    }
                }
            }
           try{
               String query = 'select ';
               for(String s:keyList)
                   query+=s+',';
               query = query.removeEnd(',');
               query+= ' from Account where Id=\''+a.id+'\' LIMIT 1';
               Account  acc = database.query(query);
               system.debug('without pretty'+acc);
               String content=JSON.serialize(acc);
               System.debug('Return data:'+content);
               String end_point ='http://192.168.1.133:8080/MTAManagement/save-salesforce';
               String username ='aimdek';
               String password = 'aimdek';
               String reqMethod= 'POST';
               RestCallouts.JLRCallout(end_point, username, password , content, a.id, reqMethod);
           }catch(Exception ex){
               System.debug('Exception:'+ex.getMessage());
           }
       }
   }
}