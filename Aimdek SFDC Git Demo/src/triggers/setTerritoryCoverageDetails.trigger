trigger setTerritoryCoverageDetails on Territory_Coverage__c (before insert, before update) {
    
    for(Territory_Coverage__c c : trigger.new) {
        String coverageKey1 = (c.City__c!=NULL?c.City__c:'')+''+(c.Province__c!=NULL?c.Province__c:'');
        String coverageKey2 = (c.City__c!=NULL?c.City__c:'')+''+(c.Province__c!=NULL?c.Province_Code__c:'');
        c.Coverage_Key__c=coverageKey1.toUpperCase();
        c.Coverage_Key_Short__c=coverageKey2.toUpperCase();
    }

}