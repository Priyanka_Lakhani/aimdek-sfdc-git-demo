<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>ClaimAutoReponse</fullName>
        <description>ClaimAutoReponse</description>
        <protected>false</protected>
        <recipients>
            <field>Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>salesforce@clubautoltd.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/ClaimAutoReponseTemplate</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_LastStatusChanged_Date</fullName>
        <field>Last_Status_Changed_Date__c</field>
        <formula>Now()</formula>
        <name>Update LastStatusChanged Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>ClaimReceived</fullName>
        <actions>
            <name>ClaimAutoReponse</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Claim__c.CreatedById</field>
            <operation>equals</operation>
            <value>Claim Site Site Guest User</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>ClaimStatusChanged</fullName>
        <actions>
            <name>Update_LastStatusChanged_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
