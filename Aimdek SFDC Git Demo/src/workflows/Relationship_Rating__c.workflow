<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Relationship_UID</fullName>
        <field>UID__c</field>
        <formula>TEXT(Year__c)&amp;&quot;-&quot;&amp;CASESAFEID(Contact__c)</formula>
        <name>Set Relationship UID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Relationship - Set UID</fullName>
        <actions>
            <name>Set_Relationship_UID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
